public class WizardTest3Controller {
@AuraEnabled 
    public static List<LeadListWrapper> fetchAccountWrapper(){     
        List<LeadListWrapper> lstLeadListWrapper = new List<LeadListWrapper>();
        // query account records and create 'LeadListWrapper' class instance for each record. 
        for(Lead acc : [Select id,Name,Phone,Email
                           From Lead
                           ORDER BY Name Limit 1000]){
                               // by default checkbox should be false 
                               lstLeadListWrapper.add(new LeadListWrapper(false,acc));
                           } 
        // return the 'lstLeadListWrapper' list 
        return lstLeadListWrapper; 
    }
    
    /* wrapper class */  
    public class LeadListWrapper {
        @AuraEnabled public boolean isChecked {get;set;}
        @AuraEnabled public  Lead objAccount{get;set;}
        public LeadListWrapper(boolean isChecked, Lead objAccount){
            this.isChecked = isChecked;
            this.objAccount = objAccount;
        }
    }

    @AuraEnabled
    public static List<emailTemplates> getEmailTemaltes(){
        System.debug('Test Log');
        List<emailTemplates> TemplateList = new List<emailTemplates>();
        for(sobject emailtemp : [ SELECT Id, FolderId, HtmlValue, FolderName, isActive, body,Name,subject FROM EmailTemplate where isActive= true]){   
            System.debug('Test Log');             
            EmailTemplates template = new EmailTemplates();
            template.emailTemplateId = String.valueOf(emailtemp.get('Id'));
            template.emailTemplatename = String.valueOf(emailtemp.get('Name'));
            template.emailbody = (emailtemp.get('HtmlValue') != null ? String.valueOf(emailtemp.get('HtmlValue')) : '' );
            template.emailSubject = String.valueOf(emailtemp.get('subject'));
            TemplateList.add(template);
        }
        return TemplateList;
    }

    public class emailTemplates{
        
        @AuraEnabled 
        public String emailTemplateId{get;set;}
        
        @AuraEnabled 
        public String emailbody{get;set;}
        
        @AuraEnabled
        public String emailSubject {get;set;}
        
        @AuraEnabled 
        public String emailTemplatename{get;set;}
    } 
    @AuraEnabled
    public static List<String> getAllFields(){
        List<String> fieldList = new List<String>();
        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Schema.SObjectType sobjType = gd.get('Lead'); 
        Schema.DescribeSObjectResult describeResult = sobjType.getDescribe(); 
        Map<String,Schema.SObjectField> fieldsMap = describeResult.fields.getMap(); 
        
        for(string str: fieldsMap.keySet()){
            // fieldList.add(fieldsMap.get(str).getDescribe().getLabel());                
            // fieldList.add(fieldsMap.get(str).getDescribe().getName());
            String refVar = fieldsMap.get(str).getDescribe().getLabel() + ':::' + fieldsMap.get(str).getDescribe().getName();
            fieldList.add(refVar);
        }

        return fieldList;      
    }

    @AuraEnabled
    public static String SendMailsMethod(List<Id> IdList , String Subject, String MailBody,String FileName,String FileType,Blob FileBlob){
        try {        
            System.debug('FileName-->'+FileName);
            System.debug('FileType--->'+FileType);
            System.debug('FileBlob---->'+FileBlob);    

            String FileBlobStr = EncodingUtil.base64Encode(FileBlob);
            System.debug('File String Blob--->'+FileBlobStr);

            List<Lead> LeadList = Database.query(Util.queryData('Lead','',' WHERE Id =: IdList'));

            Map<Id,String> LeadMailMap = new Map<Id,String>();
            LeadMailMap = changeLead(MailBody,IdList);
            List<Task> TaskList = new List<Task>();
            for(Lead l: LeadList){
                String Sendto = l.FirstName+' '+l.LastName + '' + '<' + l.Email + '>';
                // GoogleGmailApi.sendGmail(Subject,LeadMailMap.get(l.Id),Sendto);
                GoogleGmailApi.sendGmailWithAttachmentBlob(Subject,LeadMailMap.get(l.Id),Sendto,FileBlob,FileName,FileType);

                Task TaskRec = new Task();
                TaskRec.WhoId = l.Id;
                TaskRec.Subject = 'Email';
                TaskRec.Status = 'Completed';
                TaskList.add(TaskRec);

            }
            insert TaskList;

            return 'Emails Sent Successfully!!';

        } catch (Exception e) {
            // throw new AuraHandledException(e.getMessage());
            return e.getMessage();
        }
    }

    @AuraEnabled
    public static string getPreview(Id LeadId,String MailBody){
        try {
            List<Lead> LeadList = Database.query(Util.queryData('Lead','',' WHERE Id =: LeadId LIMIT 1'));
            List<Id> LeadIdList = new List<Id>();
            for(Lead c: LeadList){
                LeadIdList.add(c.Id);
            }
            Map<Id,String> LeadMailMap  = new Map<Id,String>();
            LeadMailMap = changeLead(MailBody,LeadIdList);
            // String updatedMailBody = changeLead(MailBody,LeadIdList);
            String updatedMailBody = LeadMailMap.get(LeadList[0].Id);
            return updatedMailBody;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public static Map<Id,String> changeLead(String MailBody,List<Id> LeadId){        
        List<Lead> LeadList = Database.query(Util.queryData('Lead','',' WHERE Id =: LeadId'));
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get('Lead').getDescribe().fields.getMap();
        List<String> ListFields = new List<String>();
        for(Schema.SObjectField sfield : fieldMap.Values()){
            ListFields.add(sfield.getDescribe().getname());
        }

        Map<Id,String> LeadMailMap = new Map<Id,String>();

        for(Lead c: LeadList){
            String refMail = MailBody;
            for(String s: ListFields){                
                String x = '{!Lead.'+s+'}';
                if(refMail.contains(x)){
                    System.debug('x---->'+x);
                    refMail = refMail.replace(x,String.valueOf(c.get(s)));
                    System.debug('Mail---->'+refMail);
                }
            }
            LeadMailMap.put(c.Id,refMail);
        }

        return LeadMailMap;

        // System.debug('MailBody---->'+MailBody);
        // System.debug('var--->'+varName);
        // Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        // Map <String, Schema.SObjectField> fieldMap = schemaMap.get('Lead').getDescribe().fields.getMap();
        // List<String> ListFields = new List<String>();
        // for(Schema.SObjectField sfield : fieldMap.Values()){
        //     ListFields.add(sfield.getDescribe().getname());
        // }
        // System.debug('ListFields--->'+ListFields.size());

        // for(String s: ListFields){
        //     String x = '{!Lead.'+s+'}';
        //     String y = '\'+'+varName+'.'+s+'+\'';
        //     System.debug('x---->'+x+'y-------->'+y);
        //     if(MailBody.contains(x)){
        //         MailBody = MailBody.replace(x,y);
        //         // MailBody = MailBody.replace(x,varName+'.'+s);
        //     }
            
        // }
        // System.debug('MailBody--->'+MailBody);
        // return MailBody;
    }
}