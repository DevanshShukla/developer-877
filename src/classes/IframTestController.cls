public class IframTestController {

    public void init(){

    }

    public IframTestController(){
    }

    public PageReference redirectToAppExchange(){
        String ConId = ApexPages.currentPage().getParameters().get('Id');
        System.debug('ConId-->'+ConId);

        if(ConId == '' || ConId == null){
            //ID NOT FOUND
            return null;
        }else{
            List<Contact> ContactList =  [SELECT Id,Name FROM Contact WHERE Id=: ConId LIMIT 1];
            System.debug('ContactList--->'+ContactList);
            ContactList[0].FeedBack__c = true;
            update ContactList;

            PageReference pr;
            pr = new PageReference('https://appexchange.salesforce.com/appxListingDetail?listingId=a0N3u00000MSEWUEA5');
            pr.setRedirect(true);
            return pr;
        }
    }
}