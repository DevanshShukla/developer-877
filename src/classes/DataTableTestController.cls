public with sharing class DataTableTestController {
 
 @AuraEnabled public List<String> result{get;set;}
 @AuraEnabled public List < Lead > returnList{get;set;}
    
 @AuraEnabled
 public static DataTableTestController getAllLead() {
     
     DataTableTestController obj = new DataTableTestController();
     String searchKey;
     Set < String > returnSetVIds = new Set < String > ();
     List < String > returnListVIds = new List < String > ();
     try{      
      obj.returnList  = [SELECT Id,Name,Email,Phone FROM Lead ORDER BY Name LIMIT 500];
      
      for (Lead prd: obj.returnList) {
        //obj.returnList.add(prd);
        returnListVIds.add(prd.Name);
      }
      system.debug(returnListVIds);
      returnSetVIds.addAll(returnListVIds);
      system.debug(returnSetVIds);
      obj.result = new List<String>();
      obj.result.addAll(returnSetVIds); 
      return obj;
  }
     catch (Exception e) {
        // "Convert" the exception into an AuraHandledException
        throw new AuraHandledException('Something went wrong: '+ e.getMessage());
    }
 }    

}