public class TestPage1Controller {

    public String conFirstName {get;set;}
    public String conLastName {get;set;}
    public String conEmail {get;set;}
    public String conPhone {get;set;}
    public String conAdd {get;set;}
    public String conCity {get;set;}
    public String conCountry {get;set;}
    public String conRegNo {get;set;}
    public String conAccName {get;set;}
    public String Blob1 {get;set;}
    public String Blob2 {get;set;}
    

    public void init(){

    }

    public TestPage1Controller(){

    }


    public void saveContactMethod(){
        System.debug('Blob1---->'+Blob1);
        System.debug('Blob2---->'+Blob2);
        Contact c = new Contact();
        c.FirstName = conFirstName;
        c.LastName = conLastName;
        c.Email = conEmail;
        c.Phone = conPhone;
        insert c;

        Attachment att=new Attachment();
        att.Body = EncodingUtil.base64Decode(Blob1);
        att.Name = c.FirstName+'VA';
        att.ContentType = 'video/webm';
        att.parentId = c.Id;
        insert att;

        Attachment att1=new Attachment();
        att1.Body = EncodingUtil.base64Decode(Blob2);
        att1.Name = c.FirstName+'SR';
        att1.ContentType = 'video/webm';
        att1.parentId = c.Id;
        insert att1;
    }

}