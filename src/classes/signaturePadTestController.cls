public class signaturePadTestController {
@AuraEnabled 
    public static void saveSign(String base64Data, String contentType) { 
        List<Account> lstAcc=[SELECT Id,Name FROM Account WHERE Name='SalesforceScool' limit 1];        
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');        
        Attachment a = new Attachment();
        a.parentId = lstAcc[0].Id;
        a.Body = EncodingUtil.base64Decode(base64Data);        
        a.Name = 'Signature.png';
        a.ContentType = contentType;        
        insert a;        
    }
}