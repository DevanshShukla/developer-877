public with sharing class SearchProductController {
    
    @AuraEnabled public List<String> result{get;set;}
    @AuraEnabled public List < Product2 > returnList{get;set;}
    
    @AuraEnabled
    public static SearchProductController fetchProductbyName(String searchKeyWord) {
        
        SearchProductController obj = new SearchProductController();
        String searchKey;
        Set < String > returnSetVIds = new Set < String > ();
        List < String > returnListVIds = new List < String > ();
        try
        {
            if(searchKeyWord.contains(','))
            {
                List<String> searchKeyCode = searchKeyWord.split(',') ;
                obj.returnList = [SELECT Id, Name, ProductCode, CreatedDate, CreatedById FROM Product2
                                  where Name in: searchKeyCode AND IsActive=TRUE LIMIT 500
                                 ];
            }
            else
            {
                
                searchKey = searchKeyWord + '%';
                obj.returnList  = [SELECT Id, Name, ProductCode, CreatedDate, CreatedById FROM Product2
                                   where Name LIKE: searchKey AND IsActive=TRUE LIMIT 500];
            }
            for (Product2 prd: obj.returnList) {
                //obj.returnList.add(prd);
                returnListVIds.add(prd.CreatedById);
            }
            system.debug(returnListVIds);
            returnSetVIds.addAll(returnListVIds);
            system.debug(returnSetVIds);
            obj.result = new List<String>();
            obj.result.addAll(returnSetVIds); 
            return obj;
        }
        catch (Exception e) {
            // "Convert" the exception into an AuraHandledException
            throw new AuraHandledException('Something went wrong: '
                                           + e.getMessage());    
        }
    }
    
    @AuraEnabled
    public static SearchProductController fetchProductbyCode(String searchKeyWord) {
        
        SearchProductController objCode = new SearchProductController();
        List<String> searchKeyCode;
        Set < String > returnSetVIds = new Set < String > ();
        List < String > returnListVIds = new List < String > ();
        String searchKey;
        try
        {
            if(searchKeyWord.contains(','))
            {
                searchKeyCode = searchKeyWord.split(',') ;
                objCode.returnList = [SELECT Id, Name, ProductCode, CreatedDate, CreatedById FROM Product2 
                                      where ProductCode in: searchKeyCode AND IsActive=TRUE LIMIT 500
                                     ];  
            }
            
            else
            {
                searchKey = searchKeyWord + '%';
                objCode.returnList = [SELECT Id, Name, ProductCode, CreatedDate, CreatedById FROM Product2
                                      where ProductCode LIKE: searchKey AND IsActive=TRUE LIMIT 500
                                     ];
            }   
            
            for (Product2 prd: objCode.returnList) {
                returnListVIds.add(prd.CreatedById);
            }
            
            returnSetVIds.addAll(returnListVIds);
            objCode.result = new List<String>();
            objCode.result.addAll(returnSetVIds); 
            return objCode;
        }
        
        catch (Exception e) {
            // "Convert" the exception into an AuraHandledException
            throw new AuraHandledException('Something went wrong: '
                                           + e.getMessage());    
        }
    }
}