public class TestPage2Controller {

    public List<String> Names {get;set;}
    
    public void init(){
        Names = new List<String>();
    }

    public TestPage2Controller(){
        init();
        getContacts();
    }

    public void getContacts(){
        List<Contact> ContactLIst = [SELECT Id,name,Email FROM Contact LIMIT 5];

        for(Contact c: ContactLIst){
            Names.add(c.Email);
        }

    }
}