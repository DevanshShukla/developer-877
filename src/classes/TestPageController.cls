public class TestPageController {
    // public Blob videoBlob {get;set;}
    public String videoBlob {get;set;}
    
    public void init(){

    }

    public void saveMethod(){
        System.debug('videoBlob--->'+videoBlob);
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.ContentLocation = 'S'; // S = Stored in Salesforce
        contentVersion.PathOnClient = 'test.webm';
        contentVersion.Title = 'test';
        // contentVersion.VersionData = videoBlob;
        contentVersion.VersionData = EncodingUtil.base64Decode(videoBlob);
        insert contentVersion;

        Attachment att=new Attachment();
        att.Body = EncodingUtil.base64Decode(videoBlob);
        att.Name = 'test1';
        att.ContentType = 'video/webm';
        att.parentId = '0032w00000c9TLvAAM';
        insert att;

        // ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        // contentDocumentLink.ContentDocumentId = contentVersion.Id;
        // contentDocumentLink.LinkedEntityId = '0032w00000c9TLvAAM';
        // contentDocumentLink.ShareType = 'I'; // Inferred permission
        // contentDocumentLink.Visibility = 'InternalUsers';
        // insert ContentDocumentLink;        
    }

    public void TestingMethod(){
        System.debug('Hey There this is testing method log');
    }
}