public class WizardTest6Controller {
@AuraEnabled 
    public static List<LeadListWrapper> fetchAccountWrapper(){     
        List<LeadListWrapper> lstLeadListWrapper = new List<LeadListWrapper>();
        // query account records and create 'LeadListWrapper' class instance for each record. 
        for(Contact acc : [Select id,Name,Phone,Email
                           From Contact
                           Limit 1000]){
                               // by default checkbox should be false 
                               lstLeadListWrapper.add(new LeadListWrapper(false,acc));
                           } 
        // return the 'lstLeadListWrapper' list 
        return lstLeadListWrapper; 
    }
    
    /* wrapper class */  
    public class LeadListWrapper {
        @AuraEnabled public boolean isChecked {get;set;}
        @AuraEnabled public  Contact objAccount{get;set;}
        public LeadListWrapper(boolean isChecked, Contact objAccount){
            this.isChecked = isChecked;
            this.objAccount = objAccount;
        }
    }

    @AuraEnabled
    public static List<emailTemplates> getEmailTemaltes(){
        List<emailTemplates> TemplateList = new List<emailTemplates>();
        for(sobject emailtemp : [ SELECT Id, FolderId, HtmlValue, FolderName, isActive, body,Name,subject FROM EmailTemplate where isActive= true]){                
            EmailTemplates template = new EmailTemplates();
            template.emailTemplateId = String.valueOf(emailtemp.get('Id'));
            template.emailTemplatename = String.valueOf(emailtemp.get('Name'));
            template.emailbody = (emailtemp.get('HtmlValue') != null ? String.valueOf(emailtemp.get('HtmlValue')) : '' );
            template.emailSubject = String.valueOf(emailtemp.get('subject'));
            TemplateList.add(template);
        }
        return TemplateList;
    }

    public class emailTemplates{
        
        @AuraEnabled 
        public String emailTemplateId{get;set;}
        
        @AuraEnabled 
        public String emailbody{get;set;}
        
        @AuraEnabled
        public String emailSubject {get;set;}
        
        @AuraEnabled 
        public String emailTemplatename{get;set;}
    } 
}