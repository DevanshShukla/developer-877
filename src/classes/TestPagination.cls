public class TestPagination {
    //INITIALIZE VARIABLES
    public List<String> ProList {get;set;}
    public Integer rec {get;set;}
    public Integer tot {get;set;}
    
    Integer totalRecs = 0;
    Integer count = 0;
    Integer LimitSize = 10;
    
    //CONSTUCTOR
    public TestPagination(){
        // List<String> liststring = new list<String>();
        // for(Integer i =1;i<=100;i++){
        //     liststring.add(string.valueOf(i ));
        // }
        // ProList = liststring;
        
        tot=totalRecs = [select count() from Contact];
    }

    public List<Contact> leads=new List<Contact>();
    
    public List<Contact> getLeads() {
        List<Contact> ld=[select Name from Contact LIMIT:limitsize OFFSET:count];
        system.debug('values are:' + ld);
        return ld;
    }
    
    public void updatePage() {
        leads.clear();
        limitsize=rec;
        leads=[select Name from Contact LIMIT:rec OFFSET:count];
    }
    
    public PageReference Firstbtn() {
        count=0;
        return null;
    }
      
    public PageReference prvbtn() {
        count=count-limitsize;
        return null;
    }
   
    public PageReference Nxtbtn() {
        count=count+limitsize;
        return null;
    }
    
    public PageReference lstbtn() {
        count= totalrecs - math.mod(totalRecs,LimitSize);
        return null;
    }

    public Boolean getNxt() {
        if((count+ LimitSize) > totalRecs){
            return true;
        }else{
            return false;
        }
        
    }
     public Boolean getPrv() {
      if(count== 0){
          return true;
      }else{
          return false;
      }
     }
}