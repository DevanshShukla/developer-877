public with sharing class DataTableWithPagenation {
    
    @AuraEnabled
    public static AccountPagerWrapper fetchAccount(Decimal pageNumber ,Integer recordToDisply) {
        
        Integer pageSize = recordToDisply;
        Integer offset = ((Integer)pageNumber - 1) * pageSize;
        
        // create a instance of wrapper class.
        AccountPagerWrapper obj =  new AccountPagerWrapper();
        // set the pageSize,Page(Number), total records and accounts List(using OFFSET)   
        //obj.pageSize = pageSize;
        If(pageSize == 0 ){
            obj.pageSize = 1;
        }else{
            obj.pageSize = pageSize;
        }
        obj.page = (Integer) pageNumber;
        obj.total = [SELECT count() FROM Contact];
        //obj.accounts = [SELECT Id, Name,Phone FROM Account ORDER BY Name LIMIT :recordToDisply OFFSET :offset];
        obj.contacts = [SELECT Id, Name, FirstName, LastName ,Phone FROM Contact ORDER BY Name LIMIT :recordToDisply OFFSET :offset];
        // return the wrapper class instance .
        return obj;
    }
    
    @AuraEnabled 
    public static List<contact> fetchContacts(String searchKeyWord) {
        
        String searchKey = searchKeyWord + '%';
        List<Contact> returnList = new List < Contact > ();
        List<Contact> lstOfContact = [SELECT id, Name, FirstName, LastName, Phone FROM Contact
                                         WHERE Name LIKE: searchKey LIMIT 500];
        
        for (Contact acc: lstOfContact) {
            returnList.add(acc);
        }
        return returnList;
    }
    
    // create a wrapper class with @AuraEnabled Properties    
    public class AccountPagerWrapper {
        @AuraEnabled public Integer pageSize {get;set;}
        @AuraEnabled public Integer page {get;set;}
        @AuraEnabled public Integer total {get;set;}
        @AuraEnabled public List<contact> contacts {get;set;}
    }
    
    
}