({
    getValueFromApplicationEvent : function(cmp, event) {
        var ShowResultValue = event.getParam("Pass_Result");
        console.log({ShowResultValue});
        // set the handler attributes based on event data
        cmp.set("v.Get_Result", ShowResultValue);
    }
})