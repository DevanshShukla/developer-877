({  
    showcsvdata :  function (component, event, helper){
        component.set('v.progress', 0);
        var fileInput = component.find("file").get("v.files");
        var fileName = 'No File Selected..';
        var file = fileInput[0];
        if (event.getSource().get("v.files").length > 0) {
            var fName = event.getSource().get("v.files")[0]['name'];
        }
        
        if(fName.indexOf(".csv") !== -1){           
            component.set("v.fileName", fileName);
        	if (file) {
                component.set("v.showcard", true);                
                var tabledata=[];
                var reader = new FileReader();
                reader.readAsText(file, "UTF-8");
                reader.onload = function (evt) {
                    var csv = evt.target.result;
                    if(csv.length > 4000000){
                        helper.showToast("Info", "Info!", "Please upload smaller size of csv file");
                    }else{
                        var rows = csv.split("\n");
                        console.log({rows});
                        var trimrow = rows[0].split(",");
                        console.log({trimrow});                
                        component.set("v.header",trimrow);                    
                        for (var i = 1; i < rows.length; i++) {
                            tabledata.push(rows[i]);
                        }
                        for(var j=0;j<=100;j++){
                            task(j);                                
                        }
                        function task(j) { 
                          setTimeout(function() { 
                        	component.set('v.progress',j);
                          }, 2*j);
                        }
                        console.log({tabledata});
                        component.set("v.tabledata",tabledata);  
                        var fileName = fName;
                        component.set("v.fileName",fileName);    
                    }
                                        
                }
            }
        }else{    
            helper.showToast("Info", "Info!", "Please upload only CSV file");
        }
    },     
})