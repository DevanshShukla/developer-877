({
   init: function (cmp, event, helper) {
        cmp.set('v.mycolumns', [            
            { label: 'Name', fieldName: 'Name', sortable: true,type: 'text'},
            { label: 'Email', fieldName: 'Email', sortable: true,type: 'text'}            
        ]);
		helper.SearchHelper(cmp,event);
    },
    
    updateSelectedText: function (cmp, event) {
        var selectedRows = event.getParam('selectedRows');
        cmp.set('v.selectedRowsCount', selectedRows.length);
    },        
    
    updateColumnSorting: function (cmp, event, helper) {
        cmp.set('v.isLoading', true);
        // We use the setTimeout method here to simulate the async
        // process of the sorting data, so that user will see the
        // spinner loading when the data is being sorted.
        setTimeout(function() {
            var fieldName = event.getParam('fieldName');
            var sortDirection = event.getParam('sortDirection');
            cmp.set("v.sortedBy", fieldName);
            cmp.set("v.sortedDirection", sortDirection);
            helper.sortData(cmp, fieldName, sortDirection);
            cmp.set('v.isLoading', false);
        }, 0);
    },
    handleSort: function(cmp,event,helper){
        helper.handleSort(cmp,event);
    }
    
})