({
    
    doInit : function(component, event, helper){
        // this function call on the component load first time     
        // get the page Number if it's not define, take 1 as default
        
        var page = component.get("v.page") || 1;
        
        
        var recordToDisply;   
        if( component.find("recordSize").get("v.value") == 0){
            recordToDisply = 1;
        }else{
         recordToDisply = component.find("recordSize").get("v.value") || 10;
        }
        
        component.set('v.mycolumns', [
            {label: 'Contact Name', fieldName: 'Name', type: 'Name',sortable:'true'},
            {label: 'First Name', fieldName: 'FirstName', type: 'Name',sortable:'true'},
            {label: 'Last Name', fieldName: 'lastName', type: 'Name',sortable:'true'},
            {label: 'Phone', fieldName: 'Phone', type: 'Phone',sortable:'true'}
        ]);
        
        // call the helper function   
        helper.getAccounts(component, page, recordToDisply);
    },
    
    getAccounts: function(component, page, recordToDisply) {
        
        // create a server side action. 
        var action = component.get("c.fetchAccount");
        // set the parameters to method 
        action.setParams({
            "pageNumber": page,
            "recordToDisply": recordToDisply
        });
        // set a call back   
        action.setCallback(this, function(a) {
            // store the response return value (wrapper class insatance)  
            var result = a.getReturnValue();
            console.log('result ---->' + JSON.stringify(result));
            // set the component attributes value with wrapper class properties.   
            
            component.set("v.Contacts", result.contacts);
            component.set("v.page", result.page);
            component.set("v.total", result.contacts.length);
            component.set("v.pages", Math.ceil(result.total / recordToDisply));
            
        });
        // enqueue the action 
        $A.enqueueAction(action);
    },
    
    
    SearchHelper: function(component, event) {
        // show spinner message
        var action = component.get("c.fetchContacts");
        action.setParams({
            searchKeyWord: component.get("v.searchKeyword")
        });
        action.setCallback(this, function(response) {

            if (response.getState() === "SUCCESS") {
                component.set('v.Contacts',response.getReturnValue());
                component.set("v.Message", response.getReturnValue().length == 0 ? true : false);
                component.set("v.total", response.getReturnValue().length);
                
                component.find("Id_spinner").set("v.class" , 'slds-hide');
            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted');
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " + 
                              errors[0].message);
                    }
                } else {
                    alert("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
})