({
    doInit: function(component, event, helper) {
        helper.doInit(component, event, helper);
    },
    
    navigate: function(component, event, helper) {
        // this function call on click on the previous page button  
        var page = component.get("v.page") || 1;
        // get the previous button label  
        var direction = event.getSource().get("v.label");
        
        var recordToDisply;   
        if( component.find("recordSize").get("v.value") == 0){
            recordToDisply = 1;
        }else{
         recordToDisply = component.find("recordSize").get("v.value") || 10;
        }
        
        // set the current page,(using ternary operator.)  
        page = direction === "Previous Page" ? (page - 1) : (page + 1);
        // call the helper function
        helper.getAccounts(component, page, recordToDisply); 
        
    },
    
    onSelectChange: function(component, event, helper) {
        // this function call on the select opetion change,     
        var page = 1;
        var recordToDisply = 10;
        
        if( component.find("recordSize").get("v.value") == 0){
            recordToDisply = 1;
        }else{
         recordToDisply = component.find("recordSize").get("v.value") || 10;
        }
        
        helper.getAccounts(component, page, recordToDisply);
    },
    
     Search: function(component, event, helper) {
        //var searchField = component.find('searchField');
        //var isValueMissing = searchField.get('v.validity');
        // if value is missing show error message and focus on field
        if(false) {
            searchField.showHelpMessageIfInvalid();
            searchField.focus();
        }else{
            
          // else call helper function 
            helper.SearchHelper(component, event);
        }
    },
    
})