({
    doInit : function(component, event, helper) {
        component.set('v.mycolumns', [            
            { label: 'Name', fieldName: 'Name', sortable: true,type: 'text'},
            { label: 'Phone', fieldName:'Phone', sortable: true, type: 'number'},
            { label: 'Email', fieldName: 'Email', sortable: true,type: 'text'}            
        ]);
        helper.toggleTwoAndThreeSteps(component);
        component.set("v.progressIndicatorFlag", "step1");
        helper.getAllLeads(component,event,helper);
        helper.getEmailTemplateHelper(component,event);
        helper.getfields(component,event,helper);
    },
    goToStepTwo : function(component, event, helper) {        
        var allRecords = component.get("v.listOfAllAccounts");
        console.log({allRecords});
        var selectedRecords = [];
        for (var i = 0; i < allRecords.length; i++) {
            if (allRecords[i].isChecked) {
                selectedRecords.push(allRecords[i].objAccount);
            }
        }
        if(selectedRecords.length > 0){
            helper.toggleOneAndTwoSteps(component);
            component.set("v.progressIndicatorFlag", "step2");
            component.set("v.selectedLead",selectedRecords);
        }else{            
            helper.showToast(component,event,helper,"Error","Error",0,"Please Select atleast one lead to send mail!");
        }
        // var x = JSON.stringify(selectedRecords);
    },
    goToStepThree : function(component, event, helper) {
        var subject = component.get("v.subject");
        var mailBody = component.get("v.emailbody");

        if(subject == ""){
            helper.showToast(component,event,helper,"Error","Error",0,"Please fill out subject to send mail!");
        }else if(mailBody == ""){
            helper.showToast(component,event,helper,"Error","Error",0,"Please fill out body to send mail!");
        }else{
            helper.getPreview(component,event,helper);
            helper.toggleTwoAndThreeSteps(component);
            component.set("v.progressIndicatorFlag", "step3");
        }
    },
    goBackToStepOne : function(component, event, helper) {
        helper.toggleOneAndTwoSteps(component);
        component.set("v.progressIndicatorFlag", "step1");
    },
    goBackToStepTwo : function(component, event, helper) {
        helper.toggleTwoAndThreeSteps(component);
        component.set("v.progressIndicatorFlag", "step2");
    },    
    navigation: function(component, event, helper) {
        var sObjectList = component.get("v.listOfAllAccounts");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var whichBtn = event.getSource().get("v.name");
        // check if whichBtn value is 'next' then call 'next' helper method
        if (whichBtn == 'next') {
            component.set("v.currentPage", component.get("v.currentPage") + 1);
            helper.next(component, event, sObjectList, end, start, pageSize);
        }
        // check if whichBtn value is 'previous' then call 'previous' helper method
        else if (whichBtn == 'previous') {
            component.set("v.currentPage", component.get("v.currentPage") - 1);
            helper.previous(component, event, sObjectList, end, start, pageSize);
        }
    },
    
    selectAllCheckbox: function(component, event, helper) {
        var selectedHeaderCheck = event.getSource().get("v.value");
        var updatedAllRecords = [];
        var updatedPaginationList = [];
        var listOfAllAccounts = component.get("v.listOfAllAccounts");
        var PaginationList = component.get("v.PaginationList");
        // play a for loop on all records list 
        for (var i = 0; i < listOfAllAccounts.length; i++) {
            // check if header checkbox is 'true' then update all checkbox with true and update selected records count
            // else update all records with false and set selectedCount with 0  
            if (selectedHeaderCheck == true) {
                listOfAllAccounts[i].isChecked = true;
                component.set("v.selectedCount", listOfAllAccounts.length);
            } else {
                listOfAllAccounts[i].isChecked = false;
                component.set("v.selectedCount", 0);
            }
            updatedAllRecords.push(listOfAllAccounts[i]);
        }
        // update the checkbox for 'PaginationList' based on header checbox 
        for (var i = 0; i < PaginationList.length; i++) {
            if (selectedHeaderCheck == true) {
                PaginationList[i].isChecked = true;
            } else {
                PaginationList[i].isChecked = false;
            }
            updatedPaginationList.push(PaginationList[i]);
        }
        component.set("v.listOfAllAccounts", updatedAllRecords);
        component.set("v.PaginationList", updatedPaginationList);
    },
    
    checkboxSelect: function(component, event, helper) {
        // on each checkbox selection update the selected record count 
        var selectedRec = event.getSource().get("v.value");
        var getSelectedNumber = component.get("v.selectedCount");
        if (selectedRec == true) {
            getSelectedNumber++;
        } else {
            getSelectedNumber--;
            component.find("selectAllId").set("v.value", false);
        }
        component.set("v.selectedCount", getSelectedNumber);
        // if all checkboxes are checked then set header checkbox with true   
        if (getSelectedNumber == component.get("v.totalRecordsCount")) {
            component.find("selectAllId").set("v.value", true);
        }
    },
    
    getSelectedRecords: function(component, event, helper) {
        var allRecords = component.get("v.listOfAllAccounts");
        var selectedRecords = [];
        for (var i = 0; i < allRecords.length; i++) {
            if (allRecords[i].isChecked) {
                selectedRecords.push(allRecords[i].objAccount);
            }
        }
        alert(JSON.stringify(selectedRecords));
    },
    onSelectEmailTemplate: function (component, event, helper) {
        var emailTempId = event.target.value;
        var emailbody = '';
        var emailSubject = '';
        component.set("v.templateIDs", emailTempId);
        if (emailTempId != null && emailTempId != '' && emailTempId != 'undefined') {
            var emailTemplateList = component.get("v.emailTemplateList");
            emailTemplateList.forEach(function (element) {
                if (element.emailTemplateId == emailTempId && element.emailbody != null) {
                    emailbody = element.emailbody;
                    emailSubject = element.emailSubject;
                }
            });
        }
        component.set("v.emailbody", emailbody);
        component.set("v.subject", emailSubject);
    },

    AddfieldToMailBody: function(component,event,helper){
        var valueOfField = component.find("FieldsList").get("v.value");
        if(valueOfField != ""){
            var SubjectValue = component.get("v.emailbody");
            var AddField = '{!Lead.'+valueOfField+'}';
            SubjectValue += AddField;
            component.set("v.emailbody",SubjectValue);
        }else{
            helper.showToast(component,event,helper,"Error","Error",0,"Please Select atleast one field to add into body!");
        }
        
    },

    SendMails: function(Component,event,helper){
        helper.SendMailsHelper(Component,event,helper);
    },
    fileDetails: function(component, event, helper){
		// This will contain the List of File uploaded data and status.
        console.log('In the File Details');
		var uploadFile = event.getSource().get("v.files");
		var self = this;
		var file = uploadFile[0]; // getting the first file, loop for multiple files
		var reader = new FileReader();
		reader.onload =  $A.getCallback(function() {
			var dataURL = reader.result;
            console.log({dataURL});
			var base64 = 'base64,';
			var dataStart = dataURL.indexOf(base64) + base64.length;
			dataURL= dataURL.substring(dataStart);
            console.log({dataURL});
            component.set("v.fileBlob",dataURL);
            component.set("v.fileObj",file);
            var fileBlob = component.get("v.fileBlob");
            var fileObj = component.get("v.fileObj");
            console.log({fileBlob});
            console.log({fileObj});
			// helper.upload(component, file, dataURL);
		});
		reader.readAsDataURL(file);
        console.log({reader});
	},    
})