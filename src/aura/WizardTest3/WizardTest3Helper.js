//buildNewAccountOverrideHelper.js
({
	toggleOneAndTwoSteps : function(component) {
		var stepOne = component.find("stepOne");
        $A.util.toggleClass(stepOne, 'slds-hide');
        var stepTwo = component.find("stepTwo");
        $A.util.toggleClass(stepTwo, 'slds-hide');
	},
    toggleTwoAndThreeSteps : function(component){
        var stepTwo = component.find("stepTwo");
        $A.util.toggleClass(stepTwo, 'slds-hide');
        var stepThree = component.find("stepThree");
        $A.util.toggleClass(stepThree, 'slds-hide');
    },
    // navigate to next pagination record set   
    next : function(component,event,sObjectList,end,start,pageSize){
        var Paginationlist = [];
        var counter = 0;
        for(var i = end + 1; i < end + pageSize + 1; i++){
            if(sObjectList.length > i){ 
                if(component.find("selectAllId").get("v.value")){
                    Paginationlist.push(sObjectList[i]);
                }else{
                    Paginationlist.push(sObjectList[i]);  
                }
            }
            counter ++ ;
        }
        start = start + counter;
        end = end + counter;
        component.set("v.startPage",start);
        component.set("v.endPage",end);
        component.set('v.PaginationList', Paginationlist);
    },
    // navigate to previous pagination record set   
    previous : function(component,event,sObjectList,end,start,pageSize){
        var Paginationlist = [];
        var counter = 0;
        for(var i= start-pageSize; i < start ; i++){
            if(i > -1){
                if(component.find("selectAllId").get("v.value")){
                    Paginationlist.push(sObjectList[i]);
                }else{
                    Paginationlist.push(sObjectList[i]); 
                }
                counter ++;
            }else{
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        component.set("v.startPage",start);
        component.set("v.endPage",end);
        component.set('v.PaginationList', Paginationlist);
    },
    getAllLeads: function(component,event,helper){
       var action = component.get("c.fetchAccountWrapper");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var oRes = response.getReturnValue();
                if(oRes.length > 0){
                    component.set('v.listOfAllAccounts', oRes);
                    var pageSize = component.get("v.pageSize");
                    var totalRecordsList = oRes;
                    var totalLength = totalRecordsList.length ;
                    component.set("v.totalRecordsCount", totalLength);
                    component.set("v.startPage",0);
                    component.set("v.endPage",pageSize-1);
                    
                    var PaginationLst = [];
                    for(var i=0; i < pageSize; i++){
                        if(component.get("v.listOfAllAccounts").length > i){
                            PaginationLst.push(oRes[i]);    
                        } 
                    }
                    component.set('v.PaginationList', PaginationLst);
                    component.set("v.selectedCount" , 0);
                    //use Math.ceil() to Round a number upward to its nearest integer
                    component.set("v.totalPagesCount", Math.ceil(totalLength / pageSize));    
                }else{
                    // if there is no records then display message
                    component.set("v.bNoRecordsFound" , true);
                } 
            }
            else{
                alert('Error...');
            }
        });
        $A.enqueueAction(action);    
    },
        showToast : function(component, event, helper,title,type,time,message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type" : type,
            "duration" : time,
            "message": message

        });
        toastEvent.fire();
    },
    getEmailTemplateHelper: function (component, event) {

        var action = component.get("c.getEmailTemaltes");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null) {
                console.log('....');
                component.set("v.emailTemplateList", response.getReturnValue());
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);

    },
    getfields: function(component, event, helper) { 
        var action = component.get("c.getAllFields");
        // var userObj=component.find("SobjectList").get("v.value");
        // action.setParams({
        //     "fld": userObj
        // });
            // Add callback behavior for when response is received
        var opts=[];
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(response.getReturnValue());
            if (state == "SUCCESS") {
                var allValues = response.getReturnValue();
                for (var i = 0; i < allValues.length; i++) {
                    var x = allValues[i].split(':::');
                    opts.push({
                        class: "optionClass",
                        label: x[0],
                        value: x[1]
                    });
            }
                    component.find("FieldsList").set("v.options", opts);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        // Send action off to be executed
        $A.enqueueAction(action);
    },

    getPreview: function(component,event,helper){
        var action = component.get("c.getPreview");
        var AllSelectedLeads = component.get("v.selectedLead");
        var Lead = AllSelectedLeads[0].Id;
        var mailB = component.get("v.emailbody");
        console.log({mailB});
        console.log({Lead});
        action.setParams({
            "LeadId" : Lead,
            "MailBody" : mailB
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null) {
                var res = response.getReturnValue();
                component.set("v.PreviewBody",res);
                console.log({res});
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        // Send action off to be executed
        $A.enqueueAction(action);
    },

    SendMailsHelper: function(component,event,helper){
        var action = component.get("c.SendMailsMethod");
        var Lead = [];
        var selectedRec = component.get("v.selectedLead");
        for(var i = 0; i< selectedRec.length;i++){
            Lead.push(selectedRec[i].Id);
        }
        console.log({Lead});
        var Sub = component.get("v.subject");
        console.log({Sub});
        var mailB = component.get("v.emailbody");
        console.log({mailB});
        var File =  component.get("v.fileObj");
        console.log({File});
        var fileBlob = component.get("v.fileBlob");
        console.log({fileBlob});
        action.setParams({
            "IdList" : Lead,
            "Subject" : Sub,
            "MailBody" : mailB,
            "FileName" : File.name,
            "FileType" : File.type,
            "FileBlob" : fileBlob
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null) {
                var res = response.getReturnValue();
                console.log({res});
                helper.showToast(component,event,helper,"Success","Success",0,res);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        // Send action off to be executed
        $A.enqueueAction(action);
    },   
    // handleSort: function(cmp, event) {
    //     var sortedBy = event.getParam('fieldName');
    //     console.log({sortedBy});
    //     var sortDirection = event.getParam('sortDirection');
    //     console.log({sortDirection});        
    //     var data = cmp.get("v.PaginationList.objAccount");
    //     console.log({data});        
    //     var cloneData = data;        
    //     cloneData.sort((this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1)));
        
    //     cmp.set('v.objClassController.returnList', cloneData);
    //     cmp.set('v.sortDirection', sortDirection);
    //     cmp.set('v.sortedBy', sortedBy);
    // },
    // sortBy: function(field, reverse, primer) {
    //     var key = primer ? function(x) {
    //               return primer(x[field]);
    //           }
    //         : function(x) {
    //               return x[field];
    //           };
    
    //     return function(a, b) {
    //         a = key(a);
    //         b = key(b);
    //         return reverse * ((a > b) - (b > a));
    //     };
    // },
})