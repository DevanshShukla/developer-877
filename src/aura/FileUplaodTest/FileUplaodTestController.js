({
	hh: function (component, event, helper) {
		// This will contain the List of File uploaded data and status
		var uploadFile = event.getSource().get("v.files");
		var self = this;
		var file = uploadFile[0]; // getting the first file, loop for multiple files
		var reader = new FileReader();
		reader.onload =  $A.getCallback(function() {
			var dataURL = reader.result;
			var base64 = 'base64,';
			var dataStart = dataURL.indexOf(base64) + base64.length;
			dataURL= dataURL.substring(dataStart);
			console.log({dataURL});
			console.log({file});
			helper.upload(component, file, dataURL);
		});
		reader.readAsDataURL(file);
	}
})